package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/actions"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8081"
	}
	http.HandleFunc("/", FinancialServicesRenderedHandler)

	// TLSPath is used for local development on SSL.
	TLSPath, TLSPathExists := os.LookupEnv("TLSPATH")
	if !TLSPathExists {
		log.Fatal(http.ListenAndServe(":"+port, nil))
	} else {
		cert := TLSPath + string(os.PathSeparator) + "server.crt"
		key := TLSPath + string(os.PathSeparator) + "server.key"
		log.Fatal(http.ListenAndServeTLS(":"+port, cert, key, nil))
	}
}

type FinancialServicesRendered struct {
	FinancialServicesProvided []string `suffix:"Financial Services Provided"`
	ChargedAmount float64 `suffix:"Charged Amount"`
	LoadTimestamps []float64 `suffix:"Load Timestamps"`
	Done bool `suffix:"Done"`
	InvoiceReceipt string `suffix:"Invoice Receipt"`
	DoneUserIDs []string `suffix:"Done User IDs"`
	CustomerName string `suffix:"Customer Name"`
	LoadUserIDs []string `suffix:"Load User IDs"`
	DoneTimestamps []float64 `suffix:"Done Timestamps"`

}

// FinancialServicesRenderedHandler translates a request into a format usable by func FinancialServicesRendered
// It should be triggered via a POST request since a body is required, but any HTTP verb is accepted.
func FinancialServicesRenderedHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("ioutil.ReadAll: %v", err.Error()), 400)
		return
	}
	var HTTPSCallRequest actions.HTTPSCallRequest
	HTTPSCallResponse := actions.HTTPSCallResponse{
		Request:     &common.POSTRequest{},
		Response:    &common.POSTResponse{},
		CreateDatas: []common.Data{},
		UpdateDatas: []common.Data{},
		DeleteDatas: []string{},
	}
	err = json.Unmarshal(body, &HTTPSCallRequest)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Unmarshal: %v", err.Error()), 400)
		return
	}
	err = DoFinancialServicesRendered(&HTTPSCallRequest, &HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("DoFinancialServicesRendered: %v", err.Error()), 500)
		return
	}
	responseBytes, err := json.Marshal(HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Marshal: %v", err.Error()), 500)
		return
	}
	_, err = w.Write(responseBytes)
	if err != nil {
		http.Error(w, fmt.Sprintf("w.Write: %v", err.Error()), 500)
		return
	}
}

// DoFinancialServicesRendered Records information about the financial services we are rendering for our customers; how much we are charging them; and whether they have paid or not.
func DoFinancialServicesRendered(request *actions.HTTPSCallRequest, response *actions.HTTPSCallResponse) error {
	var err error
	data := common.Data{}
	err = data.Init(request.CurrentData)
	if err != nil {
		return fmt.Errorf("data.Init: %w", err)
	}
	data.FirestoreID = request.Request.DataID
	FinancialServicesRendered := FinancialServicesRendered{}
	err = common.DataIntoStructure(&data, &FinancialServicesRendered)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}

	m, err := json.Marshal(FinancialServicesRendered)
	if err != nil {
		return fmt.Errorf("json.Marshal: %w", err)
	}
	fmt.Printf("%v\n", string(m))

	// Save any updates which happened to Data into the database
	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &FinancialServicesRendered, &data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(&data)
	if err != nil {
		return fmt.Errorf("allDB.updateData: %w", err)
	}
	
	return nil
}